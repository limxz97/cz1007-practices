#include <stdio.h>
#define SIZE 100
void compress2D(int data[SIZE][SIZE], int rowSize, int colSize);
int main()
{
    int data[SIZE][SIZE];
    int i,j;
    int rowSize, colSize;
    printf("Enter the array size (rowSize, colSize): \n");
    scanf("%d %d", &rowSize, &colSize);
    printf("Enter the matrix (%dx%d): \n", rowSize, colSize);
    for (i=0; i<rowSize; i++)
        for (j=0; j<colSize; j++)
            scanf("%d", &data[i][j]);
    printf("compress2D(): \n");
    compress2D(data, rowSize, colSize);
    return 0;
}
void compress2D(int data[SIZE][SIZE], int rowSize, int colSize)
{
    /* Write your program code here */
    int i, j;
    int tmpSign = data[0][0], amount = 0;
    for (i = 0; i < rowSize; i++) {
        for (j = 0; j < colSize; j++) {
            if (data[i][j] == tmpSign) {
                amount++;
            } else {
                printf("%d %d ", tmpSign, amount);
                tmpSign = data[i][j];
                amount = 0;
                j--;
            }

            if (j == colSize -1) {
                printf("%d %d ", tmpSign, amount);
            }
        }
        tmpSign = data[i+1][0];
        amount = 0;
        printf("\n");
    }
}
