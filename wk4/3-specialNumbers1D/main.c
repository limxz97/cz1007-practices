#include <stdio.h>
void specialNumbers1D(int ar[], int num, int *size);
int main()
{
    int a[20],i,size=0,num;

    printf("Enter a number (between 100 and 999): \n");
    scanf("%d", &num);
    specialNumbers1D(a, num, &size);
    printf("specialNumbers1D(): ");
    for (i=0; i<size; i++)
        printf("%d ",a[i]);
    return 0;
}
void specialNumbers1D(int ar[], int num, int *size)
{
    /* Write your code here */
    // size is 0
    int i;

    int digit1, digit2, digit3;

    for (i = 100; i < num+1; i++) {
        digit1 = i%10;
        digit2 = i/10%10;
        digit3 = i/100%10;

        digit1 = digit1 * digit1 * digit1;
        digit2 = digit2 * digit2 * digit2;
        digit3 = digit3 * digit3 * digit3;

        if (i == digit1 + digit2 + digit3) {
            ar[*size] = i;
            *size += 1;
        }
    }
}
