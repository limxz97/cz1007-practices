#include <stdio.h>
int longWordLength(char *s);
int main()
{
    char str[80];
    printf("Enter a string: \n");
    gets(str);
    printf("longWordLength(): %d\n", longWordLength(str));
    return 0;
}
int longWordLength(char *s)
{
    /* Write your code here */
    int output = 1, tmp = 1;

    if (*s == '\0') {
        return 0;
    }

    while (*s != '\0') {
        *s++;
        if ((*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z')) {
            tmp++;
        } else {
            output = (output >= tmp) ? output : tmp;
            tmp = 0;
        }
    }

    return output;
}
