#include <stdio.h>
#include <string.h>
#define N 20
char *longestStrInAr(char str[N][40], int size, int *length);
int main()
{
    int i, size, length;
    char str[N][40], first[40], last[40], *p;
    char dummychar;

    printf("Enter array size: \n");
    scanf("%d", &size);
    scanf("%c", &dummychar);
    for (i=0; i<size; i++)
    {
        printf("Enter string %d: \n", i+1);
        gets(str[i]);
    }
    p = longestStrInAr(str, size, &length);
    printf("longest: %s \nlength: %d\n", p, length);
    return 0;
}
char *longestStrInAr(char str[N][40], int size, int *length)
{
    /* Write your code here */
    int i, strIterator = 0;
    char *output = malloc(sizeof(char) * 3);
    strcpy(output, str[0]);
    *length = 0;

    // get the length of the first string
    while (str[0][strIterator] != '\0') {
        *length = *length+1;
        strIterator++;
    }

    // i = 0 is already setted as the default output
    for (i = 1; i < size; i++) {
        strIterator = 0;

        while (str[i][strIterator] != '\0') {
            strIterator ++;
        }

        if (strIterator > *length) {
            *length = strIterator;
            strcpy(output, str[i]);
        }
    }

    output[*length] = '\0';

    return output;
}
