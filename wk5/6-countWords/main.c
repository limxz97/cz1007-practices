#include <stdio.h>
int countWords(char *s);
int main()
{
    char str[50];
    printf("Enter the string: \n");
    gets(str);
    printf("countWords(): %d", countWords(str));
    return 0;
}
int countWords(char *s)
{
    /* Write your code here */

    // the ASCII character for space is 32
    unsigned int output = 1;

    if (*s == '\0') {
        return 0;
    }

    while (*s != '\0') {
        output += (*s == 32) ? 1 : 0 ;
        *s++;
    }

    return output;
}
