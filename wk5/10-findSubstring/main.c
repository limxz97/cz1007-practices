#include <stdio.h>
#define INIT_VALUE 999
int findSubstring(char *str, char *substr);
int main()
{
    char str[40], substr[40];
    int result = INIT_VALUE;
    printf("Enter the string: \n");
    gets(str);
    printf("Enter the substring: \n");
    gets(substr);
    result = findSubstring(str, substr);
    if (result == 1)
        printf("findSubstring(): Is a substring\n");
    else if ( result == 0)
        printf("findSubstring(): Not a substring\n");
    else
        printf("findSubstring(): An error\n");
    return 0;
}
int findSubstring(char *str, char *substr)
{
    /* Write your code here */
    int strIterator = 0;

    while (*str != '\0') {
        if (*str == substr[strIterator]) {
            // move the substr iterator forward
            strIterator++;
        } else if (strIterator > 0) {
            // reset the substr iterator
            strIterator = 0;

            // reverse the str pointer increment
            *str--;
        }
        *str++;

        if (substr[strIterator] == '\0') {
            // return once the entire substr has ended => an entire substr has been found
            return 1;
        }
    }

    return 0;
}
