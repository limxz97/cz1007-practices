#include <stdio.h>
int locateFirstChar(char *str, char ch);
int main()
{
    char str[40], ch;

    printf("Enter a string: \n");
    gets(str);
    printf("Enter the target character: \n");
    scanf("%c", &ch);
    printf("locateFirstChar(): %d\n", locateFirstChar(str, ch));
    return 0;
}
int locateFirstChar(char *str, char ch)
{
    /* Write your code here */
    int output = 0;
    while (*str != '\0') {
        if (*str == ch) {
            return output;
        }
        output++;
        *str++;
    }
    return -1;
}
