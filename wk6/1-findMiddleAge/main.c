#include <stdio.h>
#include <string.h>
typedef struct
{
    char name[20];
    int age;
} Person;
void readData(Person *p);
Person findMiddleAge(Person *p);
int main()
{
    Person man[3], middle;

    readData(man);
    middle = findMiddleAge(man);
    printf("findMiddleAge(): %s %d\n", middle.name, middle.age);
    return 0;
}
void readData(Person *p)
{
    /* Write your program code here */
    int i = 0;

    for (i = 0; i < 3; i++) {
        printf("Enter person %d: \n", i+1);
        scanf("%s %d", p[i].name,&p[i].age);
    }
}
Person findMiddleAge(Person *p)
{
    /* Write your program code here */
    Person output;

    if ((p[0].age > p[1].age) && (p[0].age < p[2].age)) {
        output.age = p[0].age;
        strcpy(output.name, p[0].name);
    } else if ((p[1].age > p[0].age) && (p[1].age < p[2].age)) {
        output.age = p[1].age;
        strcpy(output.name, p[1].name);
    } else {
        output.age = p[2].age;
        strcpy(output.name, p[2].name);
    }

    return output;

}
