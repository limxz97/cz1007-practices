#include <stdio.h>
#include <math.h>

int factorial (int input, int result);

int main()
{
    int n, denominator = 1;
    float x, result = 1.0, numerator = 1.0;
    printf("Enter x: \n");
    scanf("%f", &x);
    /* Write your program code here */

    for (n = 1; n < 11; n++) {
        result += pow(x, n) / factorial(n, 1);
    }

    printf("Result = %.2f\n", result);
    return 0;
}

int factorial (int input, int result) {
    if (input > 1) {
        result *= input--;
        factorial(input, result);
    } else {
        return result;
    }
}
