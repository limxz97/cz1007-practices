#include <stdio.h>
int main()
{
    char ch;
    printf("Enter a character: \n");
    scanf("%c", &ch);
    /* Write your code here */

    // compare using ASCII values

    if (ch >= '0' && ch <= '9') {
        printf("Digit\n");
    } else if (ch >= 'A' && ch <= 'Z') {
        printf("Upper case letter \n");
    } else if (ch >= 'a' && ch <= 'z') {
        printf("Lower case letter \n");
    }

    return 0;
}
