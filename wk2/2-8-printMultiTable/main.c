#include <stdio.h>
int main()
{
    int row, col;
    int i, input;
    printf("Enter an input number (between 1 and 9): \n");
    scanf("%d", &input);
    printf("Multiplication Table: \n");
    /* Write your code here */

    for (row = 0; row < input+1; row++) {
        if (row == 0) {
            // printing the first row
            for (col = 0; col < input+1; col++) {
                if (col == 0) {
                    printf("        ");
                } else {
                    printf("%8d", col);
                }
            }
        } else {
            for (col = 0; col < input+1; col++) {
                if (col == 0) {
                    // first column
                    printf("%8d", row);
                } else if (row >= col) {
                    printf("%8d", col*row);
                }
            }
        }

        printf("\n");
    }

    return 0;
}
