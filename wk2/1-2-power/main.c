#include <stdio.h>
int main()
{
    /* Write your program code here */
    double current, resistance, powerLoss;

    printf("Enter the current: \n");
    scanf("%lf", &current);

    printf("Enter the resistance: \n");
    scanf("%lf", &resistance);

    powerLoss = current * current * resistance;

    printf("The power loss: %.2lf \n", powerLoss);
    return 0;
}
