#include <stdio.h>
int countOddDigits1(int num);
void countOddDigits2(int num, int *result);
int main()
{
    int number, result=-1;

    printf("Enter the number: \n");
    scanf("%d", &number);
    printf("countOddDigits1(): %d\n", countOddDigits1(number));
    countOddDigits2(number, &result);
    printf("countOddDigits2(): %d\n", result);
    return 0;
}
int countOddDigits1(int num)
{
    /* Write your program code here */
    int output = 0;
    while (num > 0) {
        output += (num %2 == 1) ? 1 : 0;
        num /= 10;
    }
    return output;
}
void countOddDigits2(int num, int *result)
{
    /* Write your program code here */
    *result= 0;
    while (num > 0) {
        *result = *result + ((num %2 == 1) ? 1 : 0);
        num /= 10;
    }
}
